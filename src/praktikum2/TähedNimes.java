package praktikum2;

import lib.TextIO;

public class TähedNimes {

	public static void main(String[] args) {
		
		System.out.println("Mis su nimi on?");
		String nimi = TextIO.getlnString();
		int nimePikkus = nimi.length();
		System.out.println("Sinu nimes on " + nimePikkus + " tähte.");

	}
}

