package praktikum4;

import lib.TextIO;

public class Parool {

	public static void main(String[] args) {

		System.out.println("Sisesta parool:");
		String sisestatudParool = TextIO.getlnString();

		if (sisestatudParool.equals("PASSWORD")) {
			System.out.println("Õige parool.");
		} else {
			System.out.println("Vale parool.");
		}
	}
}
