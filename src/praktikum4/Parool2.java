package praktikum4;

import lib.TextIO;

public class Parool2 {

	public static void main(String[] args) {

		String õigeparool = "PASSWORD";

		while (true) {

			System.out.println("Sisesta parool:");
			String sisestatudParool = TextIO.getlnString();

			if (õigeparool.equals(sisestatudParool)) {
				System.out.println("Õige parool.");
				break;
			} else {
				System.out.println("Vale parool.");
			}
		}
		
	}

}
