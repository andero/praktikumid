package praktikum4;

import lib.TextIO;

public class CumLaude {

	public static void main(String[] args) {

		double keskmineHinne;
		double lõputööHinne;

		System.out.println("Mis on su keskmine hinne?");
		keskmineHinne = TextIO.getlnDouble();
		System.out.println("Mis hindele kaitsesid lõputöö?");
		lõputööHinne = TextIO.getlnDouble();

		if (keskmineHinne > 4.5 && lõputööHinne == 5) {
			System.out.println("Sa lõpetad cum laudega!");
		} else {
			if (keskmineHinne < 0 || keskmineHinne > 5 || lõputööHinne < 0 || lõputööHinne > 5) {
				System.out.println("Kontrolli sisestatud andmeid.");
			} else {
				System.out.println("Sa ei lõpeta cum laudega.");
			}
		}
	}

}
