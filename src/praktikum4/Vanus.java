package praktikum4;

import lib.TextIO;

public class Vanus {

	public static void main(String[] args) {

		int esimeneVanus;
		int teineVanus;

		System.out.println("Sisesta esimene vanus");
		esimeneVanus = TextIO.getlnInt();

		System.out.println("Sisesta teine vanus");
		teineVanus = TextIO.getlnInt();

		if (esimeneVanus - teineVanus > 10 || teineVanus - esimeneVanus > 10) {
			System.out.println("(Midagi veel krõbedamat)");
		} else if (esimeneVanus - teineVanus > 5 || teineVanus - esimeneVanus > 5) {
			System.out.println("(Midagi krõbedat)");
		} else if (esimeneVanus - teineVanus < 5 || teineVanus - esimeneVanus < 5) {
			System.out.println("Sobib!");
		}
	}

}
