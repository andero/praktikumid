package praktikum2;

import lib.TextIO;

/**
 * This class implements a simple program that will compute
 * the amount of interest that is earned on an investment over
 * a period of one year.  The initial amount of the investment
 * and the interest rate are input by the user.  The value of
 * the investment at the end of the year is output.  The
 * rate must be input as a decimal, not a percentage (for
 * example, 0.05 rather than 5).
 * 
 * This program requires TextIO for input. TextIO is not a 
 * standard part of Java, so it must be made available when
 * the program is compiled or run.
 */
public class Korrutis {

    public static void main(String[] args) {

        double principal;  // Esimene arv.
        double rate;       // Teine arv.
        double interest;   // Korrutis.

        System.out.print("Sisesta esimene arv: ");
        principal = TextIO.getlnDouble();

        System.out.print("Sisesta teine arv: ");
        rate = TextIO.getlnDouble();

        interest = principal * rate;       // Compute this year's interest.
        
        System.out.printf("Kahe arvu korrutis on %1f%n", interest);

    } // end of main()

} // end of class Korrutis