package praktikum2;

import praktikum1.TextIO;

public class GrupiSuurus2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Palun sisesta inimeste arv");
		int inimesteArv = TextIO.getlnInt();
				
		System.out.println("Palun sisesta grupi suurus");
		int grupiSuurus = TextIO.getlnInt();
		
		int gruppideArv = inimesteArv / grupiSuurus;
		System.out.println("Moodustada saab " + gruppideArv + " gruppi");

		int j22k = inimesteArv % grupiSuurus;
		System.out.println("Üle jääb " + j22k + " inimest");
	}

}
